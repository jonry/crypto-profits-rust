use chrono::Datelike;

use crate::models::{transaction::Transaction, transaction_type::TransactionType};

// Takes a vector of transactions, the year the profits have to be computed for and the timespan
// above which the profits become taxfree.
// To calculate all time profits set the year to 0
pub fn compute_profits_to_tax(
    transactions: Vec<Transaction>,
    year: i32,
    timespan_until_tax_free: chrono::Duration,
) -> f32 {
    let mut buy_transactions: Vec<Transaction> = transactions
        .to_vec()
        .into_iter()
        .filter(|ta| ta.transaction_type == TransactionType::Buy)
        .collect();

    let mut sell_transactions: Vec<Transaction> = transactions
        .into_iter()
        .filter(|ta| ta.transaction_type == TransactionType::Sell)
        .collect();

    let mut profit = 0f32;

    for sell_transaction in &mut sell_transactions {
        let mut buy_tas_to_remove: Vec<usize> = Vec::new();

        for (buy_ta_to_remove_index, buy_transaction) in buy_transactions.iter_mut().enumerate() {
            if sell_transaction.asset == buy_transaction.asset {
                let asset_sell_difference =
                    buy_transaction.amount_asset - sell_transaction.amount_asset;

                if asset_sell_difference > 0f32 {
                    // more bought than sold
                    // get profit of entire sell

                    let assets_sold_in_this_ta = sell_transaction.amount_asset;

                    let tmp_profit = sell_transaction.amount_asset
                        * sell_transaction.asset_market_price
                        - assets_sold_in_this_ta * buy_transaction.asset_market_price;

                    // has profit to be taxed according to timespan
                    if sell_transaction.timestamp - buy_transaction.timestamp
                        < timespan_until_tax_free
                        || year == 0
                    // year == 0 -> enable all time profit calculation
                    {
                        // was the asset sold in the selected year
                        // if year is 0 it gets added
                        if sell_transaction.timestamp.year() == year || year == 0 {
                            profit += tmp_profit;
                        }
                    }

                    buy_transaction.amount_asset -= assets_sold_in_this_ta;
                    break;
                } else if asset_sell_difference < 0f32 {
                    // more sold than bought
                    // get profit for the amount bought

                    let assets_sold_in_this_ta = buy_transaction.amount_asset;

                    let tmp_profit = assets_sold_in_this_ta * sell_transaction.asset_market_price
                        - buy_transaction.amount_asset * buy_transaction.asset_market_price;

                    // has profit to be taxed according to timespan
                    if sell_transaction.timestamp - buy_transaction.timestamp
                        < timespan_until_tax_free
                        || year == 0
                    // year == 0 -> enable all time profit calculation
                    {
                        // was the asset sold in the selected year
                        // if year is 0 it gets added
                        if sell_transaction.timestamp.year() == year || year == 0 {
                            profit += tmp_profit;
                        }
                    }

                    // remove sold assets from sell transacion
                    sell_transaction.amount_asset -= assets_sold_in_this_ta;

                    buy_tas_to_remove.push(buy_ta_to_remove_index);
                } else {
                    // as much sold as bought
                    let tmp_profit = sell_transaction.amount_asset
                        * sell_transaction.asset_market_price
                        - buy_transaction.amount_asset * buy_transaction.asset_market_price;

                    // has profit to be taxed according to timespan
                    if sell_transaction.timestamp - buy_transaction.timestamp
                        < timespan_until_tax_free
                        || year == 0
                    // year == 0 -> enable all time profit calculation
                    {
                        // was the asset sold in the selected year
                        // if year is 0 it gets added
                        if sell_transaction.timestamp.year() == year || year == 0 {
                            profit += tmp_profit;
                        }
                    }

                    buy_tas_to_remove.push(buy_ta_to_remove_index);
                }
            }
        }

        for index in buy_tas_to_remove {
            buy_transactions.remove(index);
        }
    }

    profit
}
