use strum_macros::EnumString;

#[derive(Debug, Clone, PartialEq, EnumString, strum_macros::ToString)]
pub enum TransactionType {
    #[strum(serialize = "deposit")]
    Deposit,
    #[strum(serialize = "buy", serialize = "BUY")]
    Buy,
    #[strum(serialize = "withdrawal")]
    Withdrawal,
    #[strum(serialize = "sell", serialize = "SELL")]
    Sell,
    #[strum(serialize = "transfer")]
    Transfer,
}
