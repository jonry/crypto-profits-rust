use strum_macros::EnumString;

#[derive(Debug, Clone, PartialEq, EnumString, strum_macros::ToString)]
pub enum Asset {
    #[strum(serialize = "EUR")]
    Eur,
    #[strum(serialize = "USD")]
    Usd,
    #[strum(serialize = "BTC")]
    Btc,
    #[strum(serialize = "ETH")]
    Eth,
    #[strum(serialize = "ADA")]
    Ada,
    #[strum(serialize = "LINK")]
    Link,
    #[strum(serialize = "DOT")]
    Dot,
    #[strum(serialize = "VET")]
    Vet,
    #[strum(serialize = "XTZ")]
    Xtz,
    #[strum(serialize = "BEST")]
    Best,
}
