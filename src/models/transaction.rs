use chrono::{DateTime, FixedOffset};

use super::{asset::Asset, transaction_type::TransactionType};

#[derive(Debug, Clone)]
pub struct Transaction {
    pub uuid: String,
    pub timestamp: DateTime<FixedOffset>,
    pub transaction_type: TransactionType,
    pub asset: Asset,
    pub amount_asset: f32,
    pub asset_market_price: f32,
}
