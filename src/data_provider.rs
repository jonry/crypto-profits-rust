use std::{fs::File, str::FromStr};

use chrono::FixedOffset;
use csv::{Reader, StringRecord};

use crate::models::{asset::Asset, transaction::Transaction, transaction_type::TransactionType};
use crate::Marketplace;

pub fn read_csv(file: &str, marketplace: Marketplace) -> Result<Vec<Transaction>, &'static str> {
    let rdr = match csv::Reader::from_path(file) {
        Ok(rdr) => rdr,
        Err(_) => return Err("could not open file"),
    };

    match marketplace {
        Marketplace::Bitpanda => read_bitpanda_transactions(rdr),
        Marketplace::BitpandaPro => read_bitpanda_pro_transactions(rdr),
        Marketplace::Binance => todo!(),
    }
}

macro_rules! parse_enum {
    (&$record:expr, $index:expr, $enum_type:ty) => {
        match $record.get($index) {
            Some(result) => match <$enum_type>::from_str(result) {
                Ok(result_enum) => Ok(result_enum),
                Err(_) => Err("could not convert property"),
            },
            None => Err("record is empty"),
        }
    };
}

fn read_bitpanda_transactions(mut rdr: Reader<File>) -> Result<Vec<Transaction>, &'static str> {
    let mut record = StringRecord::new();

    let mut result: Vec<Transaction> = Vec::new();

    loop {
        match rdr.read_record(&mut record) {
            Ok(could_read) => {
                if !could_read {
                    return Ok(result);
                }
            }
            Err(_) => return Err("could not read csv file"),
        }

        let uuid = match record.get(0) {
            Some(id) => id.to_string(),
            None => return Err("record for uuid is empty"),
        };
        let timestamp = parse_timestamp(&record, 1)?;
        let transaction_type = parse_enum!(&record, 2, TransactionType)?;
        let amount_asset = parse_f32(&record, 6)?;
        let asset = parse_enum!(&record, 7, Asset)?;
        let asset_market_price = parse_f32(&record, 8)?;

        result.push(Transaction {
            uuid,
            timestamp,
            transaction_type,
            asset,
            amount_asset,
            asset_market_price,
        });
    }
}

fn read_bitpanda_pro_transactions(mut rdr: Reader<File>) -> Result<Vec<Transaction>, &'static str> {
    let mut record = StringRecord::new();

    let mut result: Vec<Transaction> = Vec::new();

    loop {
        match rdr.read_record(&mut record) {
            Ok(could_read) => {
                if !could_read {
                    return Ok(result);
                }
            }
            Err(_) => return Err("could not read csv file"),
        }

        let uuid = match record.get(1) {
            Some(id) => id.to_string(),
            None => return Err("record for uuid is empty"),
        };
        let timestamp = parse_timestamp(&record, 10)?;
        let transaction_type = parse_enum!(&record, 2, TransactionType)?;
        let asset = parse_enum!(&record, 5, Asset)?;
        let amount_asset = parse_f32(&record, 4)?;
        let asset_market_price = parse_f32(&record, 6)?;

        result.push(Transaction {
            uuid,
            timestamp,
            transaction_type,
            asset,
            amount_asset,
            asset_market_price,
        });
    }
}

fn parse_timestamp(
    record: &StringRecord,
    index: usize,
) -> Result<chrono::DateTime<FixedOffset>, &'static str> {
    match record.get(index) {
        Some(ts_string) => match chrono::DateTime::parse_from_rfc3339(ts_string) {
            Ok(time) => Ok(time),
            Err(_) => Err("could not convert time string to time object"),
        },
        None => Err("record for timetamp is empty"),
    }
}

fn parse_f32(record: &StringRecord, index: usize) -> Result<f32, &'static str> {
    match record.get(index) {
        Some("-") => Ok(0f32),
        Some(amount_fiat) => match amount_fiat.parse::<f32>() {
            Ok(amount) => Ok(amount),
            Err(e) => {
                println!("{}", amount_fiat);
                println!("{}", e);
                Err("could not convert amount")
            }
        },
        None => Err("record for amount is empty"),
    }
}

// fn parse_transaction_type(record: &StringRecord) -> Result<TransactionType, &'static str> {
//     match record.get(2) {
//         Some(ts_type) => match TransactionType::from_str(ts_type) {
//             Ok(ts_type) => Ok(ts_type),
//             Err(_) => Err("could not convert transaction type")
//         },
//         None => Err("record for transaction type is empty")
//     }
// }

// fn parse_money_flow(record: &StringRecord) -> Result<MoneyFlow, &'static str>{
//     match record.get(3) {
//         Some(money_flow) => match MoneyFlow::from_str(money_flow) {
//             Ok(money_flow) => Ok(money_flow),
//             Err(_) => Err("could not convert moneyflow")
//         },
//         None => Err("record for money flow is empty")
//     }
// }
