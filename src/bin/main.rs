use std::env;

pub fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() % 2 != 0 {
        panic!("received not enough arguments");
    } else if args.len() == 1 {
        panic!("no arguments provided");
    }

    let mut histories: Vec<crypto_profits::TradeHistory> = Vec::new();

    let year: i32 = args.get(1).unwrap().to_string().parse().unwrap();

    for x in (2..args.len()).step_by(2) {
        let file_path = match args.get(x) {
            Some(x) => x,
            None => panic!("No file specified as second argument"),
        };

        let marketplace = match args.get(x + 1) {
            Some(x) => x.to_owned(),
            None => panic!("No file specified as second argument"),
        };

        let marketplace = match marketplace.as_str() {
            "bitpanda" => crypto_profits::Marketplace::Bitpanda,
            "bitpanda-pro" => crypto_profits::Marketplace::BitpandaPro,
            "binance" => crypto_profits::Marketplace::Binance,
            _ => panic!("unsupported marketplace"),
        };

        let history = crypto_profits::TradeHistory::new(file_path, marketplace);
        
        histories.push(history);
    }

    let profit =
    crypto_profits::compute_trade_profits(histories, year, chrono::Duration::days(365)).unwrap();

    println!("Profits are: {}", profit);
}
