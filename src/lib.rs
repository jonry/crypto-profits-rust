//! WARNING: USE AT YOUR OWN RISK! THE MAINTAINERS DON'T TAKE RESPONSIBILITY FOR MISCALCULATIONS!
//!
//! Computes the profits for a selected marketplace. Function needs
//! the CSV-File, the marktplace, the year the taxes have to be
//! computed for and the duration after how long the profits become
//! tax free.
//! To compute all time profits set the year to 0

pub(crate) mod compute_profits;
pub(crate) mod data_provider;
pub(crate) mod models;

use compute_profits::compute_profits_to_tax;
use models::transaction::Transaction;

pub enum Marketplace {
    Binance,
    Bitpanda,
    BitpandaPro,
}

pub struct TradeHistory {
    pub file_path: String,
    pub marketplace: Marketplace,
}

impl TradeHistory{
    pub fn new(file_path: &str, marketplace: Marketplace) -> TradeHistory {
        TradeHistory{
            file_path: file_path.to_string(),
            marketplace
        }
    }
}

pub fn compute_trade_profits(
    trade_histories: Vec<TradeHistory>,
    year: i32,
    timespan_until_tax_free: chrono::Duration,
) -> Result<f32, &'static str> {

    let mut transactions: Vec<Transaction> = Vec::new();

    for history in trade_histories {
        let mut tas = match data_provider::read_csv(&history.file_path, history.marketplace){
            Ok(res) => res,
            Err(e) => return Err(e),
        };

        transactions.append(&mut tas);
    }

    transactions.sort_by(|a, b| a.timestamp.cmp(&b.timestamp));

    Ok(compute_profits_to_tax(
        transactions,
        year,
        timespan_until_tax_free,
    ))
}
