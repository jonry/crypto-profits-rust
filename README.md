# Crypto Profits Rust
WARNING: USE AT YOUR OWN RISK! WE TAKE NO RESPONSIBILITY FOR MISCALCULATIONS!
Compute profits for given transaction history written in Rust. 

Select a CSV File containing the transaction history, a supported marketplace and the year you want to compute the taxes for.
If you want to compute all time profits set the year to 0.
Timespan until tax free is set to 1 year.

# Run it
Clone the project, modify the main.rs file and run `cargo build`. Run the executable like this: `./crypto_profits <year> <path-to-tradehistory.csv> <marketplace> (<path-to-tradehistory.csv> <marketplace>)*` 
